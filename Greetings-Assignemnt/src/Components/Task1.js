import React, { Component } from 'react';

class Task1 extends Component {

    constructor(props){
        console.log("constructoe")
        super(props);
         this.state = {
            time  : new Date().getHours,
            message: "",
            color: "green"

         }

        
    }
    componentDidMount(){
        console.log("called")
        setInterval(()=>{
            this.setState({
                time : new Date().getHours()
            })},1000)
    }
    render() {
        let {time:hours,color} = this.state;
        
        if(hours<12){
           this.setState({...this.state,message: "Good Morning from React. You are having coffee with me ",color:"green"})

        }
        else if(hours >= 12 && hours < 17){
            this.setState({...this.state,message: "Good Afternoon from React. Wanna lunch with me ",color:"orange"})
        }
        else if(hours >= 17){
            this.setState({...this.state,message: "Good Evening from React. Lets drink together",color:"red"})
        }
     
        return (
            <>
             <div>
                <h1 style={{color : color}}>{this.state.message}</h1>
               <h1>{hours}</h1>
             </div>
            </>
        );
    }
  
}

export default Task1;